<?php

$enemy1 = new Enemy();
$enemy2 = new Enemy();

$enemy1->dynset("health" , 22);
$enemy1->dynGet("health");
$enemy2->dynGet("health");

class Enemy
{
    var $force ;
    var $health ;

    function __construct() {
        $this->force = rand(1,3);
        $this->health = rand(5,15);
    }

    function dynGet($theVar){
        echo $this->$theVar;
    }

    function dynSet($theVar, $theValue){
        $this->$theVar = $theValue;
    }
}

?>