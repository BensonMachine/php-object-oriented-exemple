<?php
    //Normal Person
    $tony = new person;
    $tony->set_name("tony");
    $tony->set_age(32);

    $samantha = new person("samantha");
    $samantha->set_age(33);

    $sabelina = new person("sabelina", 33);

    //Super Hero
    $superConsctructor = new hero("superContructor",22);
    $superConsctructor->set_power("boomboompow");






    //Person
    class person {
        //Variable
        var $name,$age;

        //Contructor 0 Param + Constructor Manager
        public function __construct(){
        $args = func_get_args();
        $num = func_num_args();
            if(method_exists($this,$f = 'init_' . $num)) {
                call_user_func_array(array($this,$f),$args);
            }
        }

        //Constructor 1 Param
        public function init_1($new_name){
            $this->name = $new_name;
        }

        //Constructor 2 Param
        public function init_2($new_name, $new_age){
            $this->name = $new_name;
            $this->age = $new_age;
        }
        //Get Set

        //Name
        public function set_name($new_name){
            $this->name = $new_name;
        }
        function get_name() {
            return $this->name;
        }

        //Age
        public function set_age($new_age){
            $this->age = $new_age;
        }
        function get_age() {
            return $this->age;
        }
    }






    //Super Hero Person
    class hero extends person{
        //Variables
        var $power;
        //Get Set 

        //Power
        public function set_power($new_power) 
        {
            $this->power = $new_power;
        }
        function get_power() {
            return $this->power;
        }
    }
?>