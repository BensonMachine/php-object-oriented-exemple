//In java , translating to php


package Models;

import java.util.ArrayList;
import java.util.List;

public class DataContext {
    
/*
   ___                 
  / __|___ _  _ _ _ ___
 | (__/ _ \ || | '_(_-<
  \___\___/\_,_|_| /__/
                           
*/
                       
    public static List<Cours> listeCours = new ArrayList(){{
        add(new Cours(1, 2, 1, "photoshop","asd"));
        add(new Cours(2, 3, 1, "jsp","awd"));
    }};
    
    public static List<Cours> getListeCours() {
        return listeCours;
    }

    public static void setListeCours(List<Cours> listeCours) {
        DataContext.listeCours = listeCours;
    }
    
    public static Cours findCours(int numero){
        Cours cours;
        cours = DataContext.listeCours.stream().filter((elm)->elm.getNumero()==numero).findFirst().get();
        return cours;
    }
    
/*
  ___ _           _ _          _   
 | __| |_ _  _ __| (_)__ _ _ _| |_ 
 | _||  _| || / _` | / _` | ' \  _|
 |___|\__|\_,_\__,_|_\__,_|_||_\__|
*/
    
    public static List<Etudiant> listeEtudiants = new ArrayList(){{
        add(new Etudiant("Jonny", "Bing", 21313121, 819, 1));
        add(new Etudiant("Jackie", "Chan",415131321, 613, 2));
    }};
    
    public static List<Etudiant> getListeEtudiants(){
        return listeEtudiants;
    }
    
    public static Etudiant findEtudiant(int numero){
        Etudiant etudiant;
        etudiant = DataContext.listeEtudiants.stream().filter((elm)->elm.getNumero()==numero).findFirst().get();
        return etudiant;
    }
    

    private static List<Inscription> listeInscriptions = new ArrayList(){{
        
    }};

    public void AjouterInscription(Inscription insc){
        listeInscriptions.add(insc);
    };
}